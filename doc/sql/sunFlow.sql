/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : sunflow

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 26/05/2019 16:00:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for supms_dept
-- ----------------------------
DROP TABLE IF EXISTS `supms_dept`;
CREATE TABLE `supms_dept`  (
                               `id`          int(11)                                                 NOT NULL AUTO_INCREMENT,
                               `name`        varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
                               `pid`         int(11)                                                 NULL DEFAULT NULL,
                               `orders`      int(11)                                                 NULL DEFAULT NULL,
                               `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
                               `create_time` datetime(0)                                             NULL DEFAULT NULL,
                               `update_time` datetime(0)                                             NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1371 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_dictionary_catalog
-- ----------------------------
DROP TABLE IF EXISTS `supms_dictionary_catalog`;
CREATE TABLE `supms_dictionary_catalog`  (
  `catalog_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `catalog_name` varchar(64) CHARACTER SET gbk COLLATE gbk_bin NULL DEFAULT NULL COMMENT '字典名称',
  `catalog_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典描述',
  `catalog_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型(S-系统  U-用户)',
  `catalog_struct` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典结构(T-树形   L-列表)',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`catalog_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_dictionary_detail
-- ----------------------------
DROP TABLE IF EXISTS `supms_dictionary_detail`;
CREATE TABLE `supms_dictionary_detail`  (
  `catalog_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `data_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据编码',
  `data_value` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据值',
  `data_parent` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级数据编码',
  `data_value2` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展值',
  `data_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据描述',
  `data_order` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`catalog_code`, `data_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_permission
-- ----------------------------
DROP TABLE IF EXISTS `supms_permission`;
CREATE TABLE `supms_permission`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `parentRouter` bigint(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '权限信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_role_info
-- ----------------------------
DROP TABLE IF EXISTS `supms_role_info`;
CREATE TABLE `supms_role_info`  (
  `ROLE_CODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROLE_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `ROLE_TYPE` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'F 为系统 固有的 G 全局的 P 公用的 D 部门的 I 为项目角色 W工作量角色',
  `UNIT_CODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `IS_VALID` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROLE_DESC` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `update_Date` datetime(0) NULL DEFAULT NULL,
  `Create_Date` datetime(0) NULL DEFAULT NULL,
  `creator` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `updator` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ROLE_CODE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `supms_role_permission`;
CREATE TABLE `supms_role_permission`  (
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_code`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_role_router
-- ----------------------------
DROP TABLE IF EXISTS `supms_role_router`;
CREATE TABLE `supms_role_router`  (
  `router_id` int(11) NOT NULL,
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`router_id`, `role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_router_info
-- ----------------------------
DROP TABLE IF EXISTS `supms_router_info`;
CREATE TABLE `supms_router_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `parentId` bigint(20) NULL DEFAULT NULL,
  `orders`      int(11)                                                 NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `supms_user_dept`;
CREATE TABLE `supms_user_dept`  (
  `user_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `post_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`, `dept_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_user_info
-- ----------------------------
DROP TABLE IF EXISTS `supms_user_info`;
CREATE TABLE `supms_user_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_user_role
-- ----------------------------
DROP TABLE IF EXISTS `supms_user_role`;
CREATE TABLE `supms_user_role`  (
  `user_id` int(11) NULL DEFAULT NULL,
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supms_third_auth_user
-- ----------------------------
DROP TABLE IF EXISTS `supms_third_auth_user`;
CREATE TABLE `supms_third_auth_user`
(
    `id`           varchar(32)                                              NOT NULL COMMENT '主键id',
    `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
    `user_id`      varchar(32)                                              NOT NULL COMMENT '关联用户表(supms用户)',
    `source`       varchar(128)                                             NOT NULL COMMENT '第三方平台',
    `username`     varchar(32)                                              NOT NULL COMMENT '用户名',
    `access_token` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登陆令牌',
    `avatar`       varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
    `blog`         varchar(32)                                             DEFAULT NULL COMMENT '博客',
    `nickname`     varchar(32)                                             DEFAULT NULL COMMENT '绰号',
    `company`      varchar(32)                                             DEFAULT NULL COMMENT '公司',
    `location`     varchar(32)                                             DEFAULT NULL COMMENT '位置',
    `email`        varchar(32)                                             DEFAULT NULL COMMENT '邮件',
    `remark`       varchar(3072)                                           DEFAULT NULL COMMENT '备注',
    `gender`       varchar(32)                                             DEFAULT NULL COMMENT '性别',
    `CREATED_TIME` datetime                                                DEFAULT NULL COMMENT '首次登陆时间',
    `UPDATED_TIME` datetime                                                DEFAULT NULL COMMENT '最近一次登陆时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='第三方权限登陆信息表 用于记录第三方系统登陆本系统中记录和自己用户关联的信息。';
ALTER TABLE `supms_third_auth_user`
    MODIFY COLUMN `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名' AFTER `source`,
    MODIFY COLUMN `blog` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客' AFTER `avatar`,
    MODIFY COLUMN `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绰号' AFTER `blog`,
    MODIFY COLUMN `company` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司' AFTER `nickname`,
    MODIFY COLUMN `location` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '位置' AFTER `company`;

ALTER TABLE `supms_third_auth_user`
    MODIFY COLUMN `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件' AFTER `location`;

-- ----------------------------
-- Table structure for supms_log  日志信息表
-- ----------------------------
DROP TABLE IF EXISTS `supms_log`;
CREATE TABLE `supms_log`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `create_time`      datetime                                                DEFAULT NULL COMMENT '日志创建时间',
    `description`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志描述',
    `exception_detail` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '异常内容',
    `log_type`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '异常类型info/error',
    `method`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作方法',
    `params`           text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '操作参数',
    `request_ip`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '请求IP',
    `time`             bigint(20)                                              DEFAULT NULL COMMENT '请求时间',
    `username`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作用户',
    `opt_system`       varchar(255)                                            DEFAULT NULL COMMENT '标记日志来源',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8234
  DEFAULT CHARSET = utf8;
ALTER TABLE `supms_log`
    MODIFY COLUMN `time` datetime NULL DEFAULT NULL COMMENT '请求时间';

-- ----------------------------
-- Table structure for flow_studentapply
-- ----------------------------
DROP TABLE IF EXISTS `flow_studentapply`;
CREATE TABLE `flow_studentapply`  (
  `recordid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '记录主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '学校名称',
  `region` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '年级',
  `dateday` date NULL DEFAULT NULL COMMENT '报名日期',
  `datetime` datetime(0) NULL DEFAULT NULL COMMENT '时间',
  `bmtype` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '报名性质',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
