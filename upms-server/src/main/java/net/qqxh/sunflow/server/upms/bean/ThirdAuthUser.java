package net.qqxh.sunflow.server.upms.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Copyright (C), 2019/5/29, sunflow开发团队
 * 〈第三方登录账号信息表〉<br>
 * 〈功能详细描述〉
 *
 * @author jwy
 * @fileName: ThirdAuthUser.java
 * @date: 2019/5/29 20:18
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Builder
@Data
@TableName("supms_third_auth_user")
public class ThirdAuthUser {
    /**
     * //存放各个平台的主键Id,防止登陆名重复
     */
    @TableId(type = IdType.INPUT)
    private String id;
    private String userId;
    private String username;
    private String loginName;
    private String avatar;
    private String blog;
    private String nickname;
    private String company;
    private String location;
    private String email;
    private String remark;
    private String gender;
    private String source;
    private String accessToken;
    /**
     * 首次登陆时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 最近一次登陆时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;
}
