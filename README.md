# sunFlow

#### 介绍

sunFlow基于是基于camunda开发的工作流web系统。设计理念是易用，简单，美观，高效。 尤其注重对开发友好；
项目设计之初就考虑了前后端分离的vue开发模式。角色和路由之间的绑定跟角色和数据权限之间是不耦合的。前端开发只需要开发路由组件，
后端开发只需要开发 数据api接口。从菜单配置界面上分前端做的是左边的部分，后端是开发右边的部分，当然如果你不用工作流只用后台权限管理本框架也是一个绝佳的选择。
#### 软件架构
软件架构说明：目前项目分三个模块，camunda（工作流相关服务），upms-server(后台权限管理模块)，webApp(前端模块)
camunda模块用到camunda-7.1,spring-boot 2.0，工作流绘制工具使用的是bpmn-js进行的汉化。
upms-server模块使用spring-boot 2.0用于通过提供api的方式给前端调用来管理系统权限。
webApp模块为前端模块，系统所有的页面功能都集中在该模块中。前后端完全分离。使用的技术为vue 2.0 ,element-admin模板;
系统数据库采用mysql，缓存采用redis，
vue代码规范：格式为eslint
#### 系统截图
登录页面
![](https://gitee.com/cangjingge/sunFlow/raw/master/doc/img/login.png)
绘制流程
![](https://gitee.com/cangjingge/sunFlow/raw/master/doc/img/flow.png)
首页
![](https://gitee.com/cangjingge/sunFlow/raw/master/doc/img/home.png)
菜单管理
![](https://gitee.com/cangjingge/sunFlow/raw/master/doc/img/menu.png)
角色权限
![](https://gitee.com/cangjingge/sunFlow/raw/master/doc/img/roleauth.png)
用户管理
![](https://gitee.com/cangjingge/sunFlow/raw/master/doc/img/user.png)
#### 安装教程
1. 执行doc下面的sql脚本到mysql数据库中，配置项目的数据链接
2. 后端启动spring-boot项目Application#main
3. 前端运行cd webapp  ; npm install  ; npm run dev
#### 开发进度
目前权限架构已经完成：菜单配置，用户管理，角色配置，部门管理；工作流引擎已经集成，下一步需要抽象出一种工作流的开发模式出来
#### 开发模式
后端提供api数据接口按照已有模板格式开发即可，是spring mvc模式
前端通过vue单文件组件进行开发。所有组件放在views文件夹下